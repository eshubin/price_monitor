defmodule PriceMonitor.Repo.Migrations.Products do
  use Ecto.Migration

  def change do
    create table(:products) do
      add :external_product_id, :integer, null: false
      add :price, :integer, null: false
      add :product_name, :string, size: 128, null: false

      timestamps()
    end

    create table(:past_price_records) do
      add :product_id, references(:products), null: false
      add :price, :integer, null: false
      add :percentage_change, :float, null: false

      timestamps()
    end
  end
end
