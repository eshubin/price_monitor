defmodule PriceMonitor.Repo.Migrations.Index do
  use Ecto.Migration

  def change do
    create index(:products, :external_product_id, unique: true)
    create index(:past_price_records, :product_id)
  end
end
