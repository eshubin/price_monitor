use Mix.Config

config :price_monitor, ecto_repos: [PriceMonitor.Repo]

import_config "#{Mix.env}.exs"
