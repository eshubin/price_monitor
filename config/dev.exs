use Mix.Config

# Configure your database
config :price_monitor, PriceMonitor.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "price_monitor_dev",
  hostname: "localhost",
  pool_size: 10

config :price_monitor, :price_source, PriceMonitor.HttpSrc
config :price_monitor, :api_key, "abc123key"
config :price_monitor, :base_url, "http://pricing.proxy.beeceptor.com/"