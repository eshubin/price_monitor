use Mix.Config

# Configure your database
config :price_monitor, PriceMonitor.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "postgres",
  password: "postgres",
  database: "price_monitor_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :price_monitor, :price_source, PriceMonitor.Mock
config :price_monitor, :api_key, "abc123key"
config :price_monitor, :base_url, "https://omegapricinginc.com/pricing/records.json/"