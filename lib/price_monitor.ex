defmodule PriceMonitor do
  alias PriceMonitor.PriceSrc
  alias PriceMonitor.ProductRecord
  alias PriceMonitor.Price
  def main(_args \\ []) do
    body = PriceSrc.get()
    Enum.each(body["productRecords"], fn(r) ->
      ProductRecord.new(r)
      |> Price.save()
    end)
  end
end
