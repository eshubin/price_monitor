defmodule PriceMonitor.TimeFrame do
  @moduledoc false

  def get(time \\ Timex.today()) do
    from = Timex.shift(time, months: -1)
    format = "{YYYY}-{0M}-{0D}"
    {Timex.format!(from, format),
      Timex.format!(time, format)}
  end

end
