defmodule PriceMonitor.ProductRecord do
  @moduledoc false
  defstruct [:id, :name, :price, :discontinued]


  def new(record) do
    %PriceMonitor.ProductRecord{id: record["id"], name: record["name"], discontinued: record["discontinued"],
      price: parse_price(record["price"])}
  end

  defp parse_price(str) do
    Regex.run(~r/^\$(\d+)\.(\d\d)$/, str, capture: :all_but_first)
    |> Enum.join()
    |> String.to_integer()
  end
end
