defmodule PriceMonitor.HttpSrc do
  @moduledoc false
  @behaviour PriceMonitor.PriceSrc

  alias PriceMonitor.Url
  alias PriceMonitor.TimeFrame

  def get() do
    {url, params} = TimeFrame.get()
    |> Url.create()
    resp = HTTPoison.get!(url, [], params: params)
    Poison.decode!(resp.body)
  end

end
