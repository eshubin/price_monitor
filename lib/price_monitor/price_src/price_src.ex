defmodule PriceMonitor.PriceSrc do
  @moduledoc false

  @callback get() :: list()

  @source Application.fetch_env!(:price_monitor, :price_source)

  def get(), do: @source.get()

end
