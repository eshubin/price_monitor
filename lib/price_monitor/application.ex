defmodule PriceMonitor.Application do
  @moduledoc """
  The PriceMonitor Application Service.

  The price_monitor system business domain lives in this application.

  Exposes API to clients such as the `PriceMonitorWeb` application
  for use in channels, controllers, and elsewhere.
  """
  use Application

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    res = Supervisor.start_link([
      supervisor(PriceMonitor.Repo, []),
    ], strategy: :one_for_one, name: PriceMonitor.Supervisor)
    res
  end
end
