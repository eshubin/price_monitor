defmodule PriceMonitor.Price do
  @moduledoc false
  require Logger

  alias PriceMonitor.Repo
  alias PriceMonitor.Models.Product
  alias PriceMonitor.Models.PastPriceRecord
  alias PriceMonitor.ProductRecord

  def save(record) do
    case {Repo.get_by(Product, external_product_id: record.id), record} do
      {nil, %ProductRecord{discontinued: false} =r} ->
        Logger.info "adding a new product #{r.name}"
        Product.changeset(
          %Product{},
          %{external_product_id: r.id, price: r.price, product_name: r.name})
        |> Repo.insert!();
      {nil, %ProductRecord{discontinued: true}} ->
        :ok;
      {%Product{product_name: db_name}, %ProductRecord{name: src_name}} when db_name != src_name ->
        Logger.error "name mismatch in product #{record.id} db: #{db_name}, service: #{src_name}";
      {product, r} ->
        Repo.transaction(fn ->
          PastPriceRecord.changeset(
            %PastPriceRecord{},
            %{price: product.price, percentage_change: (r.price-product.price)/product.price*100})
          |> Ecto.Changeset.put_assoc(:product, product)
          |> Repo.insert!()
          Product.changeset(product, %{price: r.price})
          |> Repo.update!()
        end)
    end
  end

end
