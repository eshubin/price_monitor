defmodule PriceMonitor.Url do
  @base_url Application.fetch_env!(:price_monitor, :base_url)
  @moduledoc false
  def create({from, to}) do
    api_key = Application.fetch_env!(:price_monitor, :api_key)
    {@base_url, %{api_key: api_key,  start_date: from, end_date: to}}
  end

end
