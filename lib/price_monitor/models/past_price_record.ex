defmodule PriceMonitor.Models.PastPriceRecord do
  use Ecto.Schema
  import Ecto.Changeset

  alias PriceMonitor.Models.PastPriceRecord

  schema "past_price_records" do
    field :price, :integer
    field :percentage_change, :float
    belongs_to :product, PriceMonitor.Models.Product


    timestamps()
  end

  @doc false
  def changeset(%PastPriceRecord{} = record, attrs) do
    record
    |> cast(attrs, [:percentage_change, :price])
    |> validate_required([:percentage_change, :price])
  end
end
