defmodule PriceMonitor.Models.Product do
  use Ecto.Schema
  import Ecto.Changeset

  alias PriceMonitor.Models.Product

  schema "products" do
    field :external_product_id, :integer
    field :price, :integer
    field :product_name, :string
    has_many :past_price_records, PriceMonitor.Models.PastPriceRecord

    timestamps()
  end

  @doc false
  def changeset(%Product{} = product, attrs) do
    product
    |> cast(attrs, [:external_product_id, :price, :product_name])
    |> validate_required([:external_product_id, :price, :product_name])
  end
end
