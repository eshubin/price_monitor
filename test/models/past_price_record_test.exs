defmodule PastPriceRecordTest do
  @moduledoc false
  use PriceMonitor.DataCase

  alias PriceMonitor.Models.PastPriceRecord

  describe "products" do

    @valid_attrs %{percentage_change: 42.5, price: 102}
    @invalid_attrs %{percentage_change: 42.5}

    test "changeset with valid attributes" do
      changeset = PastPriceRecord.changeset(%PastPriceRecord{}, @valid_attrs)
      assert changeset.valid?
    end

    test "changeset with invalid attributes" do
      changeset = PastPriceRecord.changeset(%PastPriceRecord{}, @invalid_attrs)
      refute changeset.valid?
      assert changeset.errors == [
               price: {"can't be blank", [validation: :required]}
             ]
    end
  end
end
