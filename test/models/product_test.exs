defmodule ProductTest do
  @moduledoc false
  use PriceMonitor.DataCase

  alias PriceMonitor.Models.Product

  describe "products" do

    @valid_attrs %{external_product_id: 42, product_name: "some name", price: 102}
    @invalid_attrs %{external_product_id: 42, product_name: "some name"}

    test "changeset with valid attributes" do
      changeset = Product.changeset(%Product{}, @valid_attrs)
      assert changeset.valid?
    end

    test "changeset with invalid attributes" do
      changeset = Product.changeset(%Product{}, @invalid_attrs)
      refute changeset.valid?
      assert changeset.errors == [
               price: {"can't be blank", [validation: :required]}
             ]
    end
  end
end
