defmodule UrlTest do
  @moduledoc false

  use ExUnit.Case
  alias PriceMonitor.Url
  alias PriceMonitor.TimeFrame

  describe "url" do
    test "success" do
      assert {"https://omegapricinginc.com/pricing/records.json/",
               %{api_key: "abc123key", end_date: "2018-02-07",
                 start_date: "2018-01-07"}} = Url.create({"2018-01-07","2018-02-07"})
    end

    test "integration" do
      assert {"https://omegapricinginc.com/pricing/records.json/",
               %{api_key: "abc123key", end_date: "2018-02-07",
                 start_date: "2018-01-07"}} = Url.create(TimeFrame.get(Timex.to_date({2018, 2, 7})))
    end
  end

end
