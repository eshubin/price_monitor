defmodule TimeFrameTest do
  @moduledoc false

#  use PriceMonitor.DataCase
  use ExUnit.Case
  alias PriceMonitor.TimeFrame

  describe "timeframe" do

    test "middle of a month" do
      assert {"2018-01-07","2018-02-07"} = TimeFrame.get(Timex.to_date({2018, 2, 7}))
    end

    test "beginning of a month" do
      assert {"2018-01-01","2018-02-01"} = TimeFrame.get(Timex.to_date({2018, 2, 1}))
    end

    test "end of a month" do
      assert {"2018-01-28","2018-02-28"} = TimeFrame.get(Timex.to_date({2018, 2, 28}))
    end

    test "end of a longer month" do
      assert {"2017-12-31","2018-01-31"} = TimeFrame.get(Timex.to_date({2018, 1, 31}))
    end
    test "end of March" do
      assert {"2018-02-28","2018-03-31"} = TimeFrame.get(Timex.to_date({2018, 3, 31}))
    end
  end

end
