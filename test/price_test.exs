defmodule PriceTest do
  @moduledoc false
  use PriceMonitor.DataCase

  alias PriceMonitor.Price
  alias PriceMonitor.ProductRecord
  alias PriceMonitor.Models.Product
  alias PriceMonitor.Models.PastPriceRecord

  describe "price" do
    test "new" do
      id = 234567
      price = 4377
      name = "Black & White TV"
      %ProductRecord{
        id: id,
        name: name,
        price: price,
        discontinued: false
      }
      |> Price.save()
      p = Repo.get_by!(Product, external_product_id: id)
      assert price == p.price
      assert name == p.product_name
    end
    test "new discontinued" do
      id = 234567
      price = 4377
      name = "Black & White TV"
      %ProductRecord{
        id: id,
        name: name,
        price: price,
        discontinued: true
      }
      |> Price.save()
      assert nil == Repo.get_by(Product, external_product_id: id)
    end

    test "changed name" do
      id = 234567
      price = 4377
      name = "Black & White TV"
      Product.changeset(%Product{}, %{external_product_id: id, price: price, product_name: name})
      |> Repo.insert!

      %ProductRecord{
        id: id,
        name: "Schwarz & Weiss TV",
        price: 4500,
        discontinued: false
      }
      |> Price.save()
      p = Repo.get_by!(Product, external_product_id: id)
      assert price == p.price
      assert name == p.product_name
    end

    test "price update" do
      id = 234567
      price = 500
      name = "Black & White TV"
      Product.changeset(%Product{}, %{external_product_id: id, price: price, product_name: name})
          |> Repo.insert!

      new_price = 1000
      %ProductRecord{
        id: id,
        name: name,
        price: new_price,
        discontinued: false
      }
      |> Price.save()
      p = Repo.get_by!(Product, external_product_id: id)
      |> Repo.preload(:past_price_records)
      assert new_price == p.price
      assert name == p.product_name
      assert 1 == length(p.past_price_records)
      pp = hd(p.past_price_records)
      assert price == pp.price
      assert 100.0 == pp.percentage_change
    end

    test "discontinued price update" do
      id = 234567
      price = 500
      name = "Black & White TV"
      Product.changeset(%Product{}, %{external_product_id: id, price: price, product_name: name})
          |> Repo.insert!

      new_price = 1000
      %ProductRecord{
        id: id,
        name: name,
        price: new_price,
        discontinued: true
      }
      |> Price.save()
      p = Repo.get_by!(Product, external_product_id: id)
          |> Repo.preload(:past_price_records)
      assert new_price == p.price
      assert name == p.product_name
      assert 1 == length(p.past_price_records)
      pp = hd(p.past_price_records)
      assert price == pp.price
      assert 100.0 == pp.percentage_change
    end

    test "negative price update" do
      id = 234567
      price = 1000
      name = "Black & White TV"
      Product.changeset(%Product{}, %{external_product_id: id, price: price, product_name: name})
          |> Repo.insert!

      new_price = 500
      %ProductRecord{
        id: id,
        name: name,
        price: new_price,
        discontinued: true
      }
      |> Price.save()
      p = Repo.get_by!(Product, external_product_id: id)
          |> Repo.preload(:past_price_records)
      pp = hd(p.past_price_records)
      assert -50.0 == pp.percentage_change
    end
  end
end
