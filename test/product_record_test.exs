defmodule ProductRecordTest do
  @moduledoc false

  use ExUnit.Case

  alias PriceMonitor.ProductRecord

  describe "url" do
    test "success" do
      assert %ProductRecord{
               id: 123456,
               name: "Nice Chair",
               price: 3025,
               discontinued: false
             } = ProductRecord.new(
               %{
                 "id" => 123456,
                 "name" => "Nice Chair",
                 "price" => "$30.25",
                 "category" => "home-furnishings",
                 "discontinued" => false
               }
             )
    end
  end

end
